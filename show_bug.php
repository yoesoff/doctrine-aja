<?php
// show_bug.php
require_once "bootstrap.php";

$theBugId = $argv[1];

$bug = $entityManager->find("Bug", (int)$theBugId);

if ($bug) {
    echo "Bug: ".$bug->getDescription()."\n";
    echo "Engineer: ".$bug->getEngineer()->getName()."\n";
    echo "Reported By: ".$bug->getReporter()->getName()."\n";
} else {
   echo "bug not found \n";
}
