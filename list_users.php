<?php
// list_bugs.php
require_once "bootstrap.php";

$dql = "SELECT u FROM User u  ORDER BY u.id DESC";

$query = $entityManager->createQuery($dql);
$query->setMaxResults(30);
$users = $query->getResult();

foreach ($users as $user) {
    echo "- ". $user->getName();
    echo "\n";
}
