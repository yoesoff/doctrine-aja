<?php

/**
 * @Entity @Table(name="issues")
 **/
class Issue
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Column(type="text") **/
    protected $description;
    
    /**
     * @ManyToOne(targetEntity="Bug", inversedBy="issues")
     **/
    protected $bug;
    
    /**
     * @ManyToOne(targetEntity="Product", inversedBy="issues")
     **/
    protected $product;

    public function __toString()
    {
    	return $this->id;
    }
    
    public function getId()
    {
    	return $this->id;
    }
    
    public function setBug(Bug $bug)
    {
    	$this->bug = $bug;
    }
    
    public function getBug()
    {
    	return $this->bug;
    }
    
    
    public function setProduct(Product $product)
    {
    	$this->product = $product;
    }
    
    public function getProduct()
    {
    	return $this->product;
    }
    
    public function setDescription($description) {
    	$this->$description = $description;
    }
    
    public function getDescription() {
    	return $this->description;
    }
    
}
