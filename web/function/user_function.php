<?php

function getUser($user_id) {
	$entityManager = getEntityManager();

	$dql = "SELECT u, r, a FROM User u JOIN u.reportedBugs r JOIN u.assignedBugs a WHERE u.id = :user_id ";

	$query = $entityManager->createQuery($dql)->setParameter("user_id", $user_id);
	//$query->setMaxResults(30);
	$user = $query->getSingleResult();
	return $user;
}