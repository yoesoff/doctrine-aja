<?php

function getBug($bug_id) {
	$entityManager = getEntityManager();

	$dql = "SELECT a, b, x FROM Bug a JOIN a.engineer b JOIN a.reporter x WHERE a.id = :bug_id ";

	$query = $entityManager->createQuery($dql)->setParameter("bug_id", $bug_id);
	//$query->setMaxResults(30);
	$bug = $query->getSingleResult();
	return $bug;
}

function getAllBugs() {
	$entityManager = getEntityManager();
	
	$dql = "SELECT a, b, x FROM Bug a JOIN a.engineer b JOIN a.reporter x ORDER BY a.created DESC";
	
	$query = $entityManager->createQuery($dql);
	//$query->setMaxResults(30);
	$bugs = $query->getResult();
	return $bugs;
}

function getAllBugsByProduct($product_id) {
	$entityManager = getEntityManager();
	
	$productRepository = $entityManager->getRepository('Product');
	$product = $productRepository->find($product_id);
		
	$dql = "SELECT b, e, r, p FROM Bug b JOIN b.engineer e JOIN b.reporter r ";
	$dql = $dql . " JOIN b.products p WITH p.id = ?1";
	
	$query = $entityManager->createQuery($dql)->setParameter(1, $product->getId());

	$bugs = $query->getResult();
	return $bugs;
}

//using Issue
function getAllBugsByProduct2($product_id) {
	$entityManager = getEntityManager();

	$dql 	= "SELECT i, b, p FROM Issue i JOIN i.bug b JOIN i.product p";
	$dql 	= $dql . " where p.id = ?1";
	$query 	= $entityManager->createQuery($dql)->setParameter(1, $product_id);

	$issues = $query->getResult();
	return $issues;

}

function deleteBug($bug_id) {
	$entityManager = getEntityManager();

	$dql = "SELECT a, b, x FROM Bug a JOIN a.engineer b JOIN a.reporter x WHERE a.id = :bug_id ";

	$query = $entityManager->createQuery($dql)->setParameter("bug_id", $bug_id);
	//$query->setMaxResults(30);
	$bug = $query->getSingleResult();
	//die(var_dump($bug));
	$entityManager->remove($bug);
	$entityManager->flush();
}

