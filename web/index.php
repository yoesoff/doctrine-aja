<?php 
	require "./layout/header.php";
?>

<!-- Product -->
<div class="panel panel-default">
  <!-- Default panel contents -->

  	<div class="panel-heading">Products</div>
	<div class="panel-body">
		  	<?php 
		  		require './function/product_function.php';
		  	?>
		    <table class="table table-condensed">
		  		<tr>
					<th>id</th>
					<th>name</th>
					<th>total bugs</th>
					<th>action</th>
		  		</tr>
		  				  		
		  		<?php 
		  			foreach (getAllProducts() as $product) {
		  		?>
		  			<tr>
		  				<td><?= $product->getId() ?></td>
						<td><a href="product_view.php?product_id=<?= $product->getId() ?>"><?= $product->getName() ?></a></td>
						<td>
							<a href="bug_by_product.php?product_id=<?= $product->getId() ?>">
								<?php 
							    	echo "    - Bugs: ".count($product->getBugs())."\n";
							    ?>
						    </a>

						</td>
						<td>
							<a href="product_delete.php?product_id=<?= $product->getId() ?>">[x] delete </a>
						</td>
		  			</tr>
	  			<?php
	  				}
	  			?>
		  		</tr>
		  		
			</table>
		
	  </div>

</div>

<div class="panel panel-default">
  <!-- Default panel contents -->

  	<div class="panel-heading">Bugs</div>
	<div class="panel-body">
		  	<?php 
		  		require './function/bug_function.php';
		  	?>
		    <table class="table table-condensed">
		  		<tr>
					<th>id</th>
					<th>description</th>
					<th>created</th>
					<th>status</th>
					<th>engineer</th>
					<th>reporter</th>
					<th>Tot. products</th>
					<th>action</th>
		  		</tr>
		  				  		
		  		<?php 
		  			foreach (getAllBugs() as $bug) {
		  		?>
		  			<tr>
		  				<td><?= $bug->getId() ?></td>
						<td><a href="product_view.php?bug_id=<?= $bug->getId() ?>"><?= $bug->getDescription() ?></a></td>
						<td><?= $bug->getCreated()->format('d/m/Y'); ?></td>
						<td><?= $bug->getStatus() ?></td>
						<td><a href="user_view.php?user_id=<?= $bug->getEngineer()->getId() ?>"><?= $bug->getEngineer() ?></a></td>
						<td><a href="user_view.php?user_id=<?= $bug->getReporter()->getId() ?>"><?= $bug->getReporter() ?></a></td>
		  				<td>
		  					<a href="product_by_bug.php?bug_id=<?= $bug->getId() ?>">
		  						Products : <?= count($bug->getProducts()) ?>
		  					</a>
	  					</td>
	  					<td>
							<a href="bug_delete.php?bug_id=<?= $bug->getId() ?>">[x] delete </a>
						</td>
		  			</tr>
		  			<?php
		  				}
		  			?>
		  		</tr>
		  		
			</table>
		
	  </div>

</div>


<?php require "./layout/footer.php"?>
		
		
		
		
