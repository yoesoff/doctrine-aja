<?php 
	require "./layout/header.php";
	require './function/product_function.php';
?>

<!-- Product By Bug -->
<div class="panel panel-default">
  <!-- Default panel contents -->

  	<div class="panel-heading">Products</div>
	<div class="panel-body">

		    <table class="table table-condensed">
		  		<tr>
					<th>id</th>
					<th>Product Name</th>
					<th>total bugs</th>
		  		</tr>
		  				  		
		  		<?php 
		  			foreach (getAllProductsByBug($_GET['bug_id']) as $product) {
		  		?>
		  			<tr>
		  				<td><?= $product->getId() ?></td>
						<td><?= $product->getName() ?></td>
						<td>
						<a href="bug_by_product.php?product_id=<?= $product->getId() ?>">
						<?php 
					    	echo "    - Bugs: ".count($product->getBugs())."\n";
					    ?>
					    </a>
						</td>
		  			</tr>
	  			<?php
	  				}
	  			?>
		  		</tr>
		  		
			</table>
		
	  </div>

</div>

<!-- Products By Bug using Issue-->
<div class="panel panel-default">
  <!-- Default panel contents -->

  	<div class="panel-heading">Products</div>
	<div class="panel-body">
		    <table class="table table-condensed">
		  		<tr>
					<th>id</th>
					<th>Product Name</th>
					<th>total bugs</th>
		  		</tr>
		  				  		
		  		<?php 
		  			foreach (getAllProductsByBug2($_GET['bug_id']) as $issue) {
		  		?>
		  			<tr>
		  				<td><?= $issue->getProduct()->getId() ?></td>
						<td><?= $issue->getProduct()->getName() ?></td>
						<td>
							<a href="bug_by_product.php?product_id=<?= $issue->getProduct()->getId() ?>">
							<?php 
						    	echo "    - Bugs: ".count($issue->getProduct()->getBugs())."\n";
						    ?>
						    </a>
						    
						</td>
		  			</tr>
	  			<?php
	  				}
	  			?>
		  		</tr>
		  		
			</table>
		
	  </div>

</div>





<?php require "./layout/footer.php"?>
		
		
		
		
