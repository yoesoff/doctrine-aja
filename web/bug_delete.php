<?php 
	require "./layout/header.php";
?>

<!-- Product -->
<div class="panel panel-default">
  <!-- Default panel contents -->

  	<div class="panel-heading">Bug  Delete</div>
	<div class="panel-body">
		<?php require './function/bug_function.php'; ?>
		
		<?php 
			deleteBug($_GET['bug_id']); 
		?>
		product with id "<?php echo  $_GET['bug_id'] ?>" deleted!
	  </div>

</div>

<?php require "./layout/footer.php"?>