<?php
require_once "bootstrap.php";

$product_id =$argv[1];

$productRepository = $entityManager->getRepository('Product');
$product = $productRepository->find($product_id);

if (!$product) {
	echo "not found  \n";
	exit();
}

//die(var_dump($product));

$entityManager->remove($product);
$entityManager->flush();
echo "deleted \n";