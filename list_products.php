<?php
// list_products.php
require_once "bootstrap.php";

$productRepository = $entityManager->getRepository('Product');
$products = $productRepository->findAll();

foreach ($products as $product) {
    echo sprintf("-%s\n", $product->getName().", bugs(".count($product->getBugs()).")");
    foreach ($product->getBugs() as $bug) {
    	echo "    - Bug: ".$bug->getDescription()."\n";
    }
}
